﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_26
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = new string[5] { "Red", "Blue", "Yellow", "Green", "Pink" };

            Array.Sort(colours);
            Array.Reverse(colours);

            Console.WriteLine(string.Join(",", colours));
            Console.ReadLine();
        }
    }
}
