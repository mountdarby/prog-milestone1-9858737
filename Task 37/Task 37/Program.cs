﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_37
{
    class Program
    {
        static void Main(string[] args)
        {
            double hoursOfStudy = 10;
            double contactHours = 5;
            double weeksOfStudy = 12;
            double credits = 15;
            double totalHours = (credits * hoursOfStudy);
            double classTime = (weeksOfStudy * contactHours);
            double self = (totalHours - classTime);
            double weeklyStudyHoursPerPaper = (self / weeksOfStudy);

            Console.WriteLine($" For each Credit of a paper, {hoursOfStudy} hours of study are required.");
            Console.WriteLine($" There are {weeksOfStudy} Weeks of study time, with {contactHours} hours per week in lectures and tutorials.");
            Console.WriteLine($" So for a {credits} credit paper, the following formula applies:");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("  Number of Credits x Hours of study = Total Study Hours.");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($" {credits} x {hoursOfStudy} = {totalHours}.  ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("  Weeks to study x Lecture and Tutorial time = Total Class time.");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($" {weeksOfStudy} x {contactHours} = {classTime}.");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("  Total Study Hours - Total Class Time = Total Self Directed Study time. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($" {totalHours} - {classTime} = {self}.");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("  Total Self Directed Study time / Weeks to study = Weekly study hours per paper. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($" {self} / {weeksOfStudy} = {weeklyStudyHoursPerPaper}");
            Console.ForegroundColor = ConsoleColor.White;

        }
    }
}
