﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            double GST = 1.15;
            double bread = 2.05;
            double milk = 3.25;
            double cheese = 8.62;
            Console.WriteLine($"Bread is ${bread} plus gst per loaf.");
            Console.WriteLine($"Milk is ${milk} plus gst for a 2 litre bottle");
            Console.WriteLine($"Cheese is ${cheese} plus gst per kilo");
            Console.WriteLine("How many loaves of bread would you like?");
            var loaves = double.Parse(Console.ReadLine());
            var breadtotal = (bread * loaves) * GST;
            Console.WriteLine("Ok, and how many bottles of milk?");
            var bottles = double.Parse( Console.ReadLine());
            var milktotal = (milk * bottles) * GST;
            Console.WriteLine("Perfect.");
            Console.WriteLine("And how many kilo's of cheese?");
            var kilos = double.Parse(Console.ReadLine());
            var cheesetotal = (cheese * kilos) * GST;
            Console.WriteLine($"Ok, your Bread comes to ${breadtotal}");
            Console.WriteLine($"And your Milk comes to ${milktotal}");
            Console.WriteLine($"Your Cheese comes to ${cheesetotal}");
            Console.WriteLine($"Making a grand total of${breadtotal + milktotal + cheesetotal}.");
            Console.WriteLine($"Have a great rest of your day.");
                Console.ReadLine();
        }
    }
}
