﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_24
{
    class Program
    {
        static void Main(string[] args)
        {

            var menu = "m";



            do
            {

                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("  ________________________________ ");
                Console.WriteLine(" |                                |");
                Console.WriteLine(" |           Super Menu           |");
                Console.WriteLine(" |________________________________|");
                Console.WriteLine("                                   ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("   1. Option Blue. (1)                ");
                Console.WriteLine("   2. Option Green. (2)                ");
                Console.WriteLine("   3. Option Yellow. (3)                ");
                Console.WriteLine("                                   ");
                Console.WriteLine("   Please select an option :       ");

                menu = Console.ReadLine();



                if (menu == "1")
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("");
                    Console.WriteLine("  Press m to return to the main menu");
                    menu = Console.ReadLine();
                }
                else if (menu == "2")
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("");
                    Console.WriteLine("  Press m to return to the main menu");
                    menu = Console.ReadLine();
                }

                else if (menu == "3")
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("");
                    Console.WriteLine("  Press m to return to the main menu");
                    menu = Console.ReadLine();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Clear();
                    Console.WriteLine(" ");
                    Console.WriteLine("                ERROR  ");
                    Console.WriteLine("");
                    Console.ReadLine();
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("  Press m to return to the main menu");
                    menu = Console.ReadLine();

                }
            } while (menu == "m");
        }
    }
}
