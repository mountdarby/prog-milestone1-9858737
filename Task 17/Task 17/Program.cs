﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var nameAge = new List<Tuple<string, int>>();
            nameAge.Add(Tuple.Create("John West", 34));
            nameAge.Add(Tuple.Create("Mary Magdolin", 28));
            nameAge.Add(Tuple.Create("Franz Verdinan", 19));

            foreach (var x in nameAge)
            {
                Console.WriteLine($"Here we have {x.Item1} who is {x.Item2}.");
            }
            Console.ReadLine();
        }
    }
}
