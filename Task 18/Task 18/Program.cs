﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var details = new List<Tuple<string, int, string>>();
            details.Add(Tuple.Create("Paul", 13, "June"));
            details.Add(Tuple.Create("Ghuri", 9, "December"));
            details.Add(Tuple.Create("Alana", 6, "November"));

            foreach (var x in details)
            {
                Console.WriteLine($"{x.Item1} was born in New Zealand on {x.Item2}th of {x.Item3}.");
            }
            Console.ReadLine();
        }
    }
}
