﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_01
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello, what is your name?");
            var userName = Console.ReadLine();
            Console.WriteLine("Hey {0}, how old are you?", userName);
            var userAge = Console.ReadLine();
            Console.WriteLine($"I have a couch nearly {userAge} years old too!");
            Console.WriteLine("Thanks alot "+ userName +". ");
            Console.WriteLine($"Thanks alot {userName}.");
            Console.WriteLine("Thanks alot {0}.", userName);
            Console.ReadLine();
        }
    }
}
