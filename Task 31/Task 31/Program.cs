﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_31
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = 0;

            Console.WriteLine(" ");
            Console.WriteLine("    Please input a Number and press Enter.");
            Console.WriteLine("    I will check if it is divisable by 3 and 4 with no remainder.");
            input = int.Parse(Console.ReadLine());

            if (input % 3 == 0)
            {
                if (input % 4 == 0)
                {
                    Console.WriteLine($" Congratulations!");
                    Console.WriteLine($"   {input} Does divide by 3 and 4 with no remainder.");
                }
                else
                {
                    Console.WriteLine($" Close.....");
                    Console.WriteLine($" {input} only divides by 3 with no remainder, not 3 and 4.");
                }
            }
            else if (input % 4 == 0)
            {
                Console.WriteLine($"  Close.....");
                Console.WriteLine($"  {input} only divides by 4 with no remainder, not 3 and 4.");
            }
            else
            {
                Console.WriteLine($"  Hmmm...");
                Console.WriteLine($"  {input} doesn't work sorry.");
            }
            Console.ReadLine();
        }

    }
}
