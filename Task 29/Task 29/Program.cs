﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_29
{
    class Program { 

        static void Main(string[] args)
        {
            var sentence = "The quick brown fox jumped over the fence";
            var count = 0;
            var seperate = sentence.Split(' ');

            foreach (var word in seperate)
            {
                count = count + 1;
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($" There are {count} words in '{sentence}'");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
