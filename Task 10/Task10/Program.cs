﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    class Program
    {
        static void Main(string[] args)
        {
            int currentyear = 2016;
            var numOfYears = 20;
            var yearMath = 0.25;
            var leapYears = numOfYears * yearMath;
            Console.WriteLine($"It is now {currentyear}");
            Console.WriteLine("A leapyear occurs every four years.");
            Console.WriteLine("1 / 4 = 0.25");
            Console.WriteLine($"So, {numOfYears} x {yearMath} = {leapYears} ");
            Console.WriteLine($"Within the next {numOfYears} years, there will be {leapYears} leap years.");
            Console.ReadLine();
        } 
    }
}
