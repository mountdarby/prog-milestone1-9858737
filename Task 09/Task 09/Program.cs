﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_09
{
    class Program
    {
        static void Main(string[] args)
        {
            var Year = new List<Tuple<int, string>>();
            Year.Add(Tuple.Create(2017, "not a leapyear"));
            Year.Add(Tuple.Create(2018, "not a leapyear"));
            Year.Add(Tuple.Create(2019, "not a leapyear"));
            Year.Add(Tuple.Create(2020, "a leapyear"));
            Year.Add(Tuple.Create(2021, "not a leapyear"));
            Year.Add(Tuple.Create(2022, "not a leapyear"));
            Year.Add(Tuple.Create(2023, "not a leapyear"));
            Year.Add(Tuple.Create(2024, "a leapyear"));
            Year.Add(Tuple.Create(2025, "not a leapyear"));
            Year.Add(Tuple.Create(2026, "not a leapyear"));
            Year.Add(Tuple.Create(2027, "not a leapyear"));
            Year.Add(Tuple.Create(2028, "a leapyear"));
            Year.Add(Tuple.Create(2029, "not a leapyear"));
            Year.Add(Tuple.Create(2030, "not a leapyear"));
            Year.Add(Tuple.Create(2031, "not a leapyear"));
            Year.Add(Tuple.Create(2032, "a leapyear"));
            Year.Add(Tuple.Create(2033, "not a leapyear"));
            Year.Add(Tuple.Create(2034, "not a leapyear"));
            Year.Add(Tuple.Create(2035, "not a leapyear"));
            Year.Add(Tuple.Create(2036, "a leapyear"));

            foreach (var x in Year)
            {
                Console.WriteLine($"The year {x.Item1} is {x.Item2}");
            }
        
            Console.ReadLine();

        }
    }
}
