﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_35
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("");
            Console.WriteLine(" Please Input Three Numbers.");
            Console.WriteLine("      each followed by pressing enter.");
            var a = double.Parse(Console.ReadLine());
            var b = double.Parse(Console.ReadLine());
            var c = double.Parse(Console.ReadLine());
            var d = ((a * b) - c);
            var e = (a + b + c);
            var f = ((a / b) * c);
            var g = (a + (b * c));
            var h = ((a * a) + (b - c));
            Console.Clear();
            Console.WriteLine("");
            Console.WriteLine($"   You gave me the numbers {a}, {b} and {c}.");
            Console.WriteLine($"  ( {a} x {b} ) - {c} = {d} ");
            Console.WriteLine($"   {a} + {b} + {c} = {e} ");
            Console.WriteLine($"  ( {a} / {b} ) x {c} = {f} ");
            Console.WriteLine($"  {a} + ( {b} x {c} ) = {g} ");
            Console.WriteLine($"  {a}x2 + ( {b} - {c} ) = {h} ");
            Console.ReadLine();


        }
    }
}
