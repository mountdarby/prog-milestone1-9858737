﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_19
{
    class Program
    {
        static void Main(string[] args)
        {
            var val = new int[7] { 34, 45, 21, 44, 67, 87, 86 };
            List<int> even = new List<int>();
            List<int> odd = new List<int>();
            foreach (int number in val)
                if (number % 2 == 0)
                {
                    even.Add(number);
                }
                else
                {
                    odd.Add(number);
                }
            Console.WriteLine($" The even numbers are {string.Join(", ", even)}");
            Console.WriteLine($" This leaves the Odd being {string.Join(",", odd)}");
        }
    }
}
