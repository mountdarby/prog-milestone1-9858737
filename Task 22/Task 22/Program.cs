﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_22
{
    class Program
    {
        static void Main(string[] args)
        {

            var dict = new Dictionary<string, string>();
            var fruitList = new List<string>();
            var VegeList = new List<string>();
            dict.Add("Carrot", "Vegetable");
            dict.Add("Apple", "Fruit");
            dict.Add("Cucumber", "Vegetable");
            dict.Add("Bannana", "Fruit");
            dict.Add("Pumpkin", "Vegetable");
            dict.Add("Potato", "Vegetable");
            dict.Add("Cherry", "Fruit");
            dict.Add("Broad Bean", "Vegetable");


            foreach (var x in dict)
            {
                if (x.Value =="Fruit")
                {
                  fruitList.Add(x.Key);
                }
                if (x.Value=="Vegetable")
                {
                    VegeList.Add(x.Key);
                }
            }
            Console.WriteLine($"   There are {fruitList.Count} Fruits in this list.");
            Console.WriteLine($"   There are {VegeList.Count} Vegetables in this list.");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"     The Fruits are {string.Join("-",fruitList)}");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.ReadLine();
            

        }
    }
}
