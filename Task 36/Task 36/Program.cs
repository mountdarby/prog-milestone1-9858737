﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_36
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 9;
            var i = 0;

            for (i = 0; i < counter; i ++ )
            {
                var a = i + 1;
                if ((a % 2 == 0))
                {
                    Console.WriteLine($"This is line number {a} and it is an Even number.");
                }
                else
                {
                    Console.WriteLine($" This is line number {a} and it is an Odd number");
                }

            }
            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" Calculation Complete");
            Console.ForegroundColor = ConsoleColor.White;
            Console.ReadLine();
        }
    }
}
